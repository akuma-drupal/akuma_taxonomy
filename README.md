# Taxonomy+ #

This module extends default taxonomy functionality.
Provides full support for Language, Author, Created and Updated for taxonomy terms.

### Dependencies ###

* Taxonomy (Core module)

### How do I get set up? ###

* Install this module

### Limitations ###

### Who do I talk to? ###

* Nikita Makarov <mesaverde228@gmail.com>
* [Issue Tracker](https://bitbucket.org/inri13666/akuma_search/issues?status=new&status=open)
* [Wiki](https://bitbucket.org/inri13666/akuma_search/wiki/Home)