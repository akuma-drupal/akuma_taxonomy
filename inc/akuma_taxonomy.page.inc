<?php
/**
 * User  : Nikita
 * E-Mail: mesaverde228@gmail.com
 *
 * @file
 * Contain handlers for page hooks
 */

function akuma_taxonomy_preprocess_page(&$variables, $type)
{
    if (($term = menu_get_object('taxonomy_term', 2)) && (!isset($variables['term']))) {
        $variables['theme_hook_suggestions'][] = 'page__taxonomy_term__' . $term->vocabulary_machine_name;
        $variables['term'] = $term;
    };
}

function akuma_taxonomy_process_page(&$variables, $type)
{
    if (isset($variables['term'])) {
        $term = $variables['term'];
        $term_wrapper = entity_metadata_wrapper('taxonomy_term', $term);
        /**
         * Do Something With term
         */
    }
}