<?php
/**
 * User  : Nikita
 * E-Mail: mesaverde228@gmail.com
 *
 * @file
 * Contain handlers for hooks views
 */
include_once DRUPAL_ROOT . '/includes/utility.inc';
/**
 * Implements hook_views_api().
 */
function akuma_taxonomy_views_api()
{
    return array(
      'api'  => 3.0,
      'path' => drupal_get_path('module', 'akuma_taxonomy') . '/views',
    );
}

/**
 * Implements hook_views_default_views().
 */
function akuma_taxonomy_views_default_views()
{
    $path = './' . drupal_get_path('module', 'akuma_taxonomy') . '/views/default_views/*.inc';
    $views = array();
    foreach (glob($path) as $views_filename) {
        require_once($views_filename);
    }

    return $views;
}

function akuma_taxonomy_views_data()
{
    $data = array();
    $data['taxonomy_term_data']['uid'] = array(
      'title'        => 'Author uid',
      'help'         => 'The user authoring the term. If you need more fields than the uid add the "Taxonomy term: Author" relationship',
      'relationship' => array(
        'title'   => 'Author',
        'help'    => 'Relate content to the user who created it.',
        'handler' => 'views_handler_relationship',
        'base'    => 'users',
        'field'   => 'uid',
        'label'   => 'author',
      ),
      'filter'       => array(
        'handler' => 'views_handler_filter_user_name',
      ),
      'argument'     => array(
        'handler' => 'views_handler_argument_numeric',
      ),
      'field'        => array(
        'handler' => 'views_handler_field_user',
      ),
    );
    $data['taxonomy_term_data']['created'] = array(
      'title'  => 'Post date',
      'help'   => 'The date the term was posted.',
      'field'  => array(
        'handler'        => 'views_handler_field_date',
        'click sortable' => true,
      ),
      'sort'   => array(
        'handler' => 'views_handler_sort_date',
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_date',
      ),
    );

    $data['taxonomy_term_data']['changed'] = array(
      'title'  => 'Updated date',
      'help'   => 'The date the term was last updated.',
      'field'  => array(
        'handler'        => 'views_handler_field_date',
        'click sortable' => true,
      ),
      'sort'   => array(
        'handler' => 'views_handler_sort_date',
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_date',
      ),
    );

    return $data;
}

function akuma_taxonomy_views_data_alter(&$data)
{
    if (db_field_exists('taxonomy_term_data', 'language')) {
        $data['taxonomy_term_data']['language'] = array(
          'title'    => 'Language',
          'help'     => 'Language of the taxonomy term',
          'field'    => array(
            'handler'        => 'views_handler_field_user_language',
            'click sortable' => true,
          ),
          'sort'     => array(
            'handler' => 'views_handler_sort',
          ),
          'filter'   => array(
            'handler' => 'views_handler_filter_node_language',
          ),
          'argument' => array(
            'handler' => 'views_handler_argument_node_language',
          ),
        );
    }
}
