<?php
/**
 * User  : Nikita
 * E-Mail: mesaverde228@gmail.com
 *
 * @file
 * Contain handlers for hooks views
 */
include_once DRUPAL_ROOT . '/includes/utility.inc';

/**
 * Alter the entity info.
 *
 * @param $entity_info
 *   The entity info array, keyed by entity name.
 *
 * @see hook_entity_info()
 */
function akuma_taxonomy_entity_info_alter(&$entity_info)
{
    if (module_exists('taxonomy')) {
        $entity_info['node']['view modes'] += array(
          'taxonomy_term_page' => array(
            'label'           => t('Taxonomy Term Page Item'),
            'custom settings' => false,
          ),
        );
    }
}

/**
 * Callback for getting taxonomy term properties.
 *
 * @see entity_metadata_node_entity_info_alter()
 */
function akuma_taxonomy_metadata_taxonomy_term_get_properties($term, array $options, $name, $entity_type)
{
    switch ($name) {
        case 'author':
            return !empty($term->uid) ? $term->uid : drupal_anonymous_user();
    }
}

/**
 * Implements hook_entity_property_info() on top of taxonomy module.
 *
 * @see entity_entity_property_info()
 */
function akuma_taxonomy_entity_property_info()
{
    $info = array();
    $properties = & $info['taxonomy_term']['properties'];
    $properties['created'] = array(
      'label'             => 'Date created',
      'type'              => 'date',
      'description'       => 'The date the term was posted.',
      'setter callback'   => 'entity_property_verbatim_set',
      'setter permission' => 'administer taxonomy',
      'schema field'      => 'created',
    );
    $properties['changed'] = array(
      'label'        => 'Date changed',
      'type'         => 'date',
      'schema field' => 'changed',
      'description'  => 'The date the term was most recently updated.',
    );
    $properties['author'] = array(
      'label'             => 'Author',
      'type'              => 'user',
      'description'       => 'The author of the term.',
      'getter callback'   => 'akuma_taxonomy_metadata_taxonomy_term_get_properties',
      'setter callback'   => 'entity_property_verbatim_set',
      'setter permission' => 'administer taxonomy',
      'required'          => true,
      'schema field'      => 'uid',
    );
    if (db_field_exists('taxonomy_term_data', 'language')) {
        $properties['language'] = array(
          'label'             => 'Language',
          'type'              => 'token',
          'description'       => 'The language the term is written in.',
          'setter callback'   => 'entity_property_verbatim_set',
          'options list'      => 'entity_metadata_language_list',
          'schema field'      => 'language',
          'setter permission' => 'administer taxonomy',
        );
    }

    return $info;
}

/**
 * Implements hook_entity_property_info_alter().
 *
 * @see entity_entity_property_info_alter()
 */
function akuma_taxonomy_entity_property_info_alter(&$entity_info)
{

}