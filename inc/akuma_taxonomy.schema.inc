<?php
/**
 * User  : Nikita
 * E-Mail: mesaverde228@gmail.com
 *
 * @file
 * Contain handlers for hooks schema
 */

/**
 *
 * Implements hook_schema().
 *
 */
function akuma_taxonomy_schema()
{
    $schema = array();

    return $schema;
}

/**
 *
 * Implements hook_schema_alter().
 *
 */
function akuma_taxonomy_schema_alter(&$schema)
{
    if (isset($schema['taxonomy_term_data'])) {
        $schema['taxonomy_term_data']['fields']['created'] = $schema['node']['fields']['created'];
        $schema['taxonomy_term_data']['fields']['changed'] = $schema['node']['fields']['changed'];
        $schema['taxonomy_term_data']['fields']['uid'] = $schema['node']['fields']['uid'];
        $schema['taxonomy_term_data']['indexes']['taxonomy_term_created'][0] = 'created';
        $schema['taxonomy_term_data']['indexes']['taxonomy_term_changed'][0] = 'changed';
    }
}