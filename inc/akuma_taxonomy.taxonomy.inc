<?php
/**
 * User  : Nikita
 * E-Mail: mesaverde228@gmail.com
 *
 * @file
 * Contain handlers for taxonomy hooks
 */
include_once DRUPAL_ROOT . '/includes/utility.inc';

function akuma_taxonomy_taxonomy_term_presave($term)
{
    $term->changed = time();
    if (!isset($term->uid)) {
        global $user;
        if ($user) {
            $term->uid = $user->uid;
        } else {
            $term->uid = 0;
        }
    }
    if (!isset($term->tid)) {
        $term->created = time();
    }
}

function akuma_taxonomy_taxonomy_term_insert($term)
{

}

function akuma_taxonomy_taxonomy_term_update($term)
{
}

function akuma_taxonomy_taxonomy_term_load($terms)
{
}


/**
 * Act on a taxonomy term that is being assembled before rendering.
 *
 * The module may add elements to $term->content prior to rendering. The
 * structure of $term->content is a renderable array as expected by
 * drupal_render().
 *
 * @param $term
 *   The term that is being assembled for rendering.
 * @param $view_mode
 *   The $view_mode parameter from taxonomy_term_view().
 * @param $langcode
 *   The language code used for rendering.
 *
 * @see hook_entity_view()
 */
function akuma_taxonomy_taxonomy_term_view($term, $view_mode, $langcode)
{
    /** Uptolike Fix */
    if (module_exists('uptolike')) {
        $term->type = $term->vocabulary_machine_name;
    }
}

/**
 * Alter the results of taxonomy_term_view().
 *
 * @see hook_entity_view_alter()
 */
function akuma_taxonomy_taxonomy_term_view_alter(&$build)
{

}

/**
 * Process variables for taxonomy-term.tpl.php.
 */
function akuma_taxonomy_preprocess_taxonomy_term(&$variables)
{

}

function akuma_taxonomy_process_taxonomy_term(&$variables)
{

}